const cheerio = require('cheerio');
const logger = require('terminal-logger')('test');
const fs = require('fs').promises;
logger.level = 'info';



const formater = (html) => {

  logger.write().status('info', 'Formating data...');

  const $ = cheerio.load(html, {
    normalizeWhitespace: true,
    decodeEntities: true
  });

  const LEAGUES = []
  const HOME = []
  const AWAY = []
  const RESULTS = []
  const SPORTS = []

  const sports = $('.table tr td.sport_n');
  const leagues = $('.table tr td.league_n');
  const home = $('.table tr td:nth-of-type(4) > a:nth-of-type(1)')
  const result = $('.table tr td:nth-of-type(6) > a')
  const away = $('.table tr td:nth-of-type(4) > a:nth-of-type(2)')


  for (let i = 0; i < sports.length; i++) {
    let item = sports.eq(i);
    SPORTS.push(item.text().trim())
  }

  for (let i = 0; i < leagues.length; i++) {
    let item = leagues.eq(i);
    LEAGUES.push(item.text().trim())
  }

  for (let i = 0; i < home.length; i++) {
    let item = home.eq(i);
    HOME.push(item.text().trim())
  }

  for (let i = 0; i < away.length; i++) {
    let item = away.eq(i);
    AWAY.push(item.text().trim())
  }

  for (let i = 0; i < result.length; i++) {
    let item = result.eq(i);
    RESULTS.push(item.text().trim())
  }


  const finalResult = SPORTS.map((sport, i) => {
    return {
      idx: i,
      sport,
      league: LEAGUES[i],
      home: HOME[i],
      away: AWAY[i],
      result: RESULTS[i]
    }
  })

  logger.write().tick('Formating data done.');

  return finalResult
}

const getPage = async (url, page) => {
  try {
    logger.write().status('info', `Go to url: ${ url }`);
    await page.goto(url, { waitUntil: 'domcontentloaded' });
    logger.write().tick('Page content ready.');
  } catch (error) {
    process.exit(1);
  }

  let html = null;
  try {
    logger.write().status('info', 'Getting page content..');
    html = await page.content();
    logger.write().tick('Page content grabbed successfully.');
  } catch (error) {
    process.exit(1);
  }

  return html
}


const saveToFile = async (path, result) => {
  try {
    logger.write().status('info', 'Saving to file...');
    await fs.writeFile(path, JSON.stringify(result, null, "  "))
    logger.write().tick('File saved successfully.');
  } catch (error) {
    process.exit(1);
  }
}


module.exports = {
  getPage,
  formater,
  saveToFile
}
