


const logger = require('terminal-logger')('test');
const puppeteer = require('puppeteer-extra'); // puppeteer extra to enable extra features
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const { formater, saveToFile, getPage } = require('./functions')


logger.level = 'info';
puppeteer.use(StealthPlugin());

(async () => {

  let browser = null;
  let page = null;
  let html = null;
  let result = null;

  const blockedResources = [
    'stylesheet',
    'image',
    'font',
    'media',
  ]


  try {
    logger.write().status('info', 'Opening browser...');

    browser = await puppeteer.launch({
      headless: true,
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox",
        "--disable-gpu",
        "--disable-dev-shm-usage",
        "--disable-accelerated-2d-canvas",
      ]
    });

    logger.write().tick('Browser opened.');
  } catch (error) {
    process.exit(1);
  }


  try {
    logger.write().status('info', 'Opening to new page...');

    page = await browser.newPage();
    page.setRequestInterception(true);
    page.on('request', (req) => {
      if (blockedResources.includes(req.resourceType())) req.abort();
      else req.continue();
    })

    logger.write().tick('Opening new page finished.');
  } catch (error) {
    process.exit(1);
  }


  html = await getPage('https://betsapi.com/e/', page)

  result = formater(html)

  await saveToFile('./results/page-1.json', result)


  html = await getPage('https://betsapi.com/e/p.2', page)

  result = formater(html)

  await saveToFile('./results/page-2.json', result)


  try {
    logger.write().status('info', 'Browser closing...');
    await browser.close();
    logger.write().tick('Browser closed');
  } catch (error) {
    process.exit(1);
  }

  logger.write().tick('Done.');
  process.exit(0);
})();
